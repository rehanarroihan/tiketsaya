package com.multazamgsd.tiketsaya;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

public class SuccessRegisterActivity extends AppCompatActivity {

    private Button btnExplore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success_register);

        btnExplore = findViewById(R.id.btnExplore);
        btnExplore.setOnClickListener(v -> startActivity(new Intent(SuccessRegisterActivity.this, HomeActivity.class)));
    }
}
