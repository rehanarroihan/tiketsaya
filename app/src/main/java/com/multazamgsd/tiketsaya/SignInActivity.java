package com.multazamgsd.tiketsaya;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class SignInActivity extends AppCompatActivity {
    private TextView tvToSignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        tvToSignUp = findViewById(R.id.tvToSignUp);
        tvToSignUp.setOnClickListener(v -> startActivity(new Intent(this, RegisterOneActivity.class)));
    }
}
