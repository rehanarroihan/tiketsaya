package com.multazamgsd.tiketsaya;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.LinearLayout;

public class RegisterOneActivity extends AppCompatActivity {

    private LinearLayout btnBack;
    private Button btnContinue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_one);

        btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(v -> finish());

        btnContinue = findViewById(R.id.btnContinue);
        btnContinue.setOnClickListener(v -> startActivity(new Intent(
                RegisterOneActivity.this, RegisterTwoActivity.class)));
    }
}
