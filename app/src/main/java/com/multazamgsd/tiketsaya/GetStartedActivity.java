package com.multazamgsd.tiketsaya;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

public class GetStartedActivity extends AppCompatActivity {

    private Button btnSignIn, btnSignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_started);

        btnSignIn = findViewById(R.id.btnSignIn);
        btnSignUp = findViewById(R.id.btnSignUp);
        btnSignIn.setOnClickListener(v ->
                startActivity(new Intent(GetStartedActivity.this, SignInActivity.class))
        );
        btnSignUp.setOnClickListener(v ->
                startActivity(new Intent(GetStartedActivity.this, RegisterOneActivity.class))
        );
    }
}
